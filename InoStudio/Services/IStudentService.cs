﻿using InoStudio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoStudio.Services
{
    interface IStudentService
    {
        Task<StudentModel> GetStudentByIdAsync(int id);
        Task<List<StudentModel>> GetAllStudentsAsync();
        Task<StudentModel> AddStudentAsync(StudentModel _subj);
        Task<StudentModel> ModifyStudentAsync(StudentModel _subj);
        Task DeleteStudentByIdAsync(int id);
    }
}
