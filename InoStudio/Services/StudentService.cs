﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using InoStudio.Models;
using InoStudio.Repositoies;

namespace InoStudio.Services
{
    public class StudentService : IStudentService
    {
        private StudentRepository _repos = new StudentRepository();

        public StudentService(StudentRepository repos)
        {
            _repos = repos;
        }
        public async Task<StudentModel> AddStudentAsync(StudentModel _subj)
        {
            return await _repos.AddStudent(_subj);
        }

        public async Task DeleteStudentByIdAsync(int id)
        {
            await _repos.DeleteStudentById(id);
        }

        public async Task<List<StudentModel>> GetAllStudentsAsync()
        {
            return await _repos.GetAllStudents();
        }

        public async Task<StudentModel> GetStudentByIdAsync(int id)
        {
            return await _repos.GetStudentById(id);
        }

        public async Task<StudentModel> ModifyStudentAsync(StudentModel _subj)
        {
            return await _repos.ModifyStudent(_subj);
        }
    }
}