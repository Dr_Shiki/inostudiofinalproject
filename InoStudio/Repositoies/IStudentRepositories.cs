﻿using InoStudio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoStudio.Repositoies
{
    interface IStudentRepository
    {
        Task<StudentModel> GetStudentById(int id);
        Task<List<StudentModel>> GetAllStudents();
        Task<StudentModel> AddStudent(StudentModel subj);
        Task DeleteStudentById(int id);
        Task<StudentModel> ModifyStudent(StudentModel subj);

    }
}
